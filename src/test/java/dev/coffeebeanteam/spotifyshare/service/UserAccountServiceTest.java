package dev.coffeebeanteam.spotifyshare.service;

import dev.coffeebeanteam.spotifyshare.dto.ui.UserAccountDto;
import dev.coffeebeanteam.spotifyshare.model.UserAccount;
import dev.coffeebeanteam.spotifyshare.repository.UserAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserAccountServiceTest {

    private UserAccountService userAccountService;
    private UserAccountRepository userAccountRepository;
    private OAuth2AuthorizedClient authorizedClient;

    @BeforeEach
    void setUp() {
        userAccountRepository = mock(UserAccountRepository.class);
        authorizedClient = mock(OAuth2AuthorizedClient.class);
        userAccountService = new UserAccountService(userAccountRepository);
    }

    @Test
    void givenAuthorizedClientIsSet_thenGetterReturnsTheSetValue() {
        userAccountService.setAuthorizedClient(authorizedClient);
        assertNotNull(userAccountService.getAuthorizedClient());
    }

    @Test
    void givenAuthorizedClientIsSet_whenLoggedInUserAccountIsCalled_thenTheExpectedUserAccountIsReturned() {
        userAccountService.setAuthorizedClient(authorizedClient);

        when(authorizedClient.getPrincipalName()).thenReturn("testPrincipal");
        UserAccount expectedAccount = new UserAccount();
        when(userAccountRepository.findBySpotifyPrincipalName(eq("testPrincipal"))).thenReturn(Optional.of(expectedAccount));

        UserAccount result = userAccountService.getLoggedInUserAccount();

        assertEquals(expectedAccount, result);
    }

    @Test
    void givenAuthorizedClientIsNotSet_whenGetLoggedInUserAccountIsCalled_thenAnExceptionIsThrown() {
        assertThrows(RuntimeException.class, () -> userAccountService.getLoggedInUserAccount());
    }

    @Test
    void givenNoMatch_whenSearchUsersByDisplayNameIsCalled_thenNoResultsAreReturned() {
        String search = "no_match";

        userAccountService.setAuthorizedClient(authorizedClient);
        UserAccount loggedInUserAccount = new UserAccount();
        loggedInUserAccount.setId(1L);

        when(authorizedClient.getPrincipalName()).thenReturn("testPrincipal");
        when(userAccountRepository.findBySpotifyPrincipalName(eq("testPrincipal"))).thenReturn(Optional.of(loggedInUserAccount));

        when(userAccountRepository.findBySpotifyUsernameContainingIgnoreCase(eq(search), any())).thenReturn(Page.empty());

        List<UserAccountDto> result = userAccountService.searchUsersByDisplayName(search);

        assertTrue(result.isEmpty());
    }

    @Test
    void givenLoggedInUserMatchesSearchCriteria_whenSearchUsersByDisplayNameIsCalled_thenLoggedInUserIsExcludedFromResults() {
        String search = "john";

        userAccountService.setAuthorizedClient(authorizedClient);
        UserAccount loggedInUserAccount = new UserAccount();
        loggedInUserAccount.setId(1L);
        loggedInUserAccount.setSpotifyUsername("john_doe");

        when(authorizedClient.getPrincipalName()).thenReturn("testPrincipal");
        when(userAccountRepository.findBySpotifyPrincipalName(eq("testPrincipal"))).thenReturn(Optional.of(loggedInUserAccount));

        List<UserAccount> accounts = Arrays.asList(loggedInUserAccount);
        Page<UserAccount> page = new PageImpl<>(accounts);

        when(userAccountRepository.findBySpotifyUsernameContainingIgnoreCase(eq(search), any())).thenReturn(page);

        List<UserAccountDto> result = userAccountService.searchUsersByDisplayName(search);

        assertTrue(result.isEmpty());
    }
}
